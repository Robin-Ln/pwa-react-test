# Une pwa

- [Mozilla](https://developer.mozilla.org/fr/docs/Web/Progressive_web_apps)
- [Google](https://web.dev/progressive-web-apps/)

## Autres documentations

- [Service worker](https://developers.google.com/web/fundamentals/primers/service-workers)
- [Mode offline / cache](https://developer.mozilla.org/fr/docs/Web/Progressive_web_apps/Offline_Service_workers)
- [Notification / consentement](https://developer.mozilla.org/fr/docs/Web/API/ServiceWorkerRegistration)
- [Notification / push](https://developer.mozilla.org/en-US/docs/Web/API/PushManager)

## Outils

- [React](https://fr.reactjs.org/docs/create-a-new-react-app.html)
- [Styled components](https://styled-components.com/)
- [Netlify](https://app.netlify.com/sites/rlo-pwa-react-test/overview)
- [Workbox](https://developers.google.com/web/tools/workbox)
- [Axios](https://axios-http.com/docs/intro)
- [React icons](https://react-icons.github.io/react-icons)
- [Favicon io](https://favicon.io/favicon-converter/)

## Netlify

- [l'App](https://rlo-pwa-react-test.netlify.app/)

## TODO

- [ ] Amélioré la ci : test, image docker (installation de Netlify)
- [ ] Notification : Envoyer des notifications push
- [ ] UX : Couleurs, layout, ...

## test image

![test](/docs/Capture2.png)

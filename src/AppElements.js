import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
* {
  padding: 0;
  margin: 0;
}

body {

  height: 100vh;
  width: 100vw;

  display: flex;
  align-items: center;
  justify-content: center;

  background-color: #4C6663;
}
`;

export const AppWrapper = styled.div`
  height: 90vh;
  width: 90vw;
  display: flex;
  align-items: center;
  justify-content: center;
`;

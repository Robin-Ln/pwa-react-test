import styled from "styled-components";

export const SliderWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 5fr 5fr 1fr;
  grid-template-areas:
    "section card card"
    "notification card card"
    "notification footer footer";

  @media screen and (max-width: 346px) {
    grid-template-rows: 200px 1fr 100px;
    grid-template-areas:
      "section section section"
      "card card card"
      "notification footer footer";
  }
`;

export const Section = styled.div`
  grid-area: section;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const SlideInfoWrapper = styled.div`
  background-color: #80cfa9;
  border-radius: 10px;
  justify-items: center;
  align-self: center;
  transition: all 0.3s ease-in-out;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2);
  opacity: ${({ isSelected }) => (isSelected ? "100%" : "0%")};

  &:hover {
    transform: scale(1.02);
    cursor: pointer;
  }
`;

export const SlideInfoContainer = styled.div`
  justify-items: center;
  align-self: center;
  padding: 30px;
`;

export const CardContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  opacity: 0;
  transition-duration: 1s ease;

  width: 92%;
  height: 92%;

  &.active {
    opacity: 1;
    transition-duration: 1s;
    transform: scale(1.08);
  }
`;

export const Card = styled.div`
  grid-area: card;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const CardImage = styled.img`
  max-width: 100%;
  max-height: 100%;
  border-radius: 10px;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2);
`;

export const Footer = styled.div`
  grid-area: footer;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const FooterContainer = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
`;

export const Notification = styled.div`
  grid-area: notification;
  display: flex;
  flex-direction: column;
`;

export const Button = styled.div`
  border-radius: 50px;
  background-color: ${({ primary }) => (primary ? "#A7E2E3" : "#C9D7F8")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? "#000" : "#fff")};
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  padding: 10px;
  margin: 10px;

  &:hover {
    background-color: ${({ primary }) => (primary ? "#D4C5E2" : "#A7E2E3")};
  }
`;

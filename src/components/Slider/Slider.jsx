import React, { useState, useEffect, useMemo } from "react";
import { BsFillCaretLeftFill, BsFillCaretRightFill } from "react-icons/bs";

import {
  Footer,
  Button,
  Card,
  Section,
  SliderWrapper,
  FooterContainer,
  CardContainer,
  CardImage,
  SlideInfoContainer,
  SlideInfoWrapper,
  Notification,
} from "./SliderElements";
import { fetchBeers } from "../../api";

const keys = {
  subject: "mailto: <robin.louarn@icloud.com>",
  publicKey:
    "BIYxOUHhgzQTXq5S1aIVFdTpf1qcu59hC5O8A2XMox0ky5bapffSNGIpOl1LbUIlEPVMIx8dAq6TNMTYHrYBst0",
  privateKey: "BaK9DuNcm6Wt0TtdeHkf-3ZmFpjLQufLJaD4WKblAgs",
};

const Slider = () => {
  const [beers, setBeers] = useState([]);
  const [selectedBeer, setSelectedBeer] = useState(0);

  useEffect(() => {
    fetchBeers().then((data) => {
      setBeers(data);
    });
  }, []);

  const selectedBeers = useMemo(
    () =>
      beers.map((beer, index) => ({
        ...beer,
        isSelected: index === selectedBeer,
      })),
    [beers, selectedBeer]
  );

  const previous = () => {
    const nextIndex = selectedBeer - 1;
    if (nextIndex >= 0) {
      setSelectedBeer(nextIndex);
    }
  };

  const next = () => {
    const nextIndex = selectedBeer + 1;
    if (nextIndex < beers.length) {
      setSelectedBeer(nextIndex);
    }
  };

  const askUserPermission = async () => {
    const result = await window.Notification.requestPermission();
    if (result === "granted") {
      navigator.serviceWorker.ready.then((registration) => {
        registration.showNotification("Vibration Sample", {
          body: "Buzz! Buzz!",
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: "vibration-sample",
        });
      });
    }
    console.log(result);
    return result;
  };

  const sendNotification = () => {
    navigator.serviceWorker.ready.then((registration) => {
      registration.showNotification("Vibration Sample", {
        body: "Buzz! Buzz!",
      });
    });
  };

  const registerPushManager = () => {
    navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
      var options = {
        userVisibleOnly: true,
        applicationServerKey: keys.publicKey,
      };
      serviceWorkerRegistration.pushManager.subscribe(options).then(
        function (pushSubscription) {
          alert(JSON.stringify(pushSubscription));
          // The push subscription details needed by the application
          // server are now available, and can be sent to it using,
          // for example, an XMLHttpRequest.
        },
        function (error) {
          // During development it often helps to log errors to the
          // console. In a production environment it might make sense to
          // also report information about errors back to the
          // application server.
          console.log(error);
        }
      );
    });
  };

  return (
    <SliderWrapper>
      <Section>
        {beers.length >= 0 &&
          selectedBeers.map((beer) => <SlideInfo key={beer.id} beer={beer} />)}
      </Section>
      {beers.length >= 0 &&
        selectedBeers.map((beer) => <Slide key={beer.id} beer={beer} />)}

      <Notification>
        <Button primary dark onClick={askUserPermission}>
          Request notifications
        </Button>
        <Button primary dark onClick={sendNotification}>
          Send notifications
        </Button>
        <Button primary dark onClick={registerPushManager}>
          Request pushManager
        </Button>
      </Notification>
      <Footer>
        <FooterContainer>
          <Button onClick={previous}>
            <BsFillCaretLeftFill />
          </Button>
          <Button onClick={next}>
            <BsFillCaretRightFill />
          </Button>
        </FooterContainer>
      </Footer>
    </SliderWrapper>
  );
};

const Slide = ({ beer }) => (
  <Card>
    <CardContainer className={beer.isSelected ? "active" : ""}>
      {beer.isSelected && <CardImage src={beer.image} />}
    </CardContainer>
  </Card>
);

const SlideInfo = ({ beer }) => (
  <SlideInfoWrapper isSelected={beer.isSelected}>
    {beer.isSelected && (
      <SlideInfoContainer>
        <h1>{beer.name}</h1>
        <p>{beer.description}</p>
      </SlideInfoContainer>
    )}
  </SlideInfoWrapper>
);

export default Slider;

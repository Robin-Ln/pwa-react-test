import React from "react";
import { AppWrapper, GlobalStyle } from "./AppElements";
import { Slider } from "./components";
const App = () => {
  return (
    <>
      <GlobalStyle />
      <AppWrapper>
        <Slider />
      </AppWrapper>
    </>
  );
};


export default App;

import axios from "axios";

const fetchBeers = async () => {
  const response = await axios({
    url: "data/beers.json",
  });
  return response.data;
};

export default fetchBeers;

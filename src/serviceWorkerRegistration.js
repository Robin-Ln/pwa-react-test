export default function registerServiceWorker() {
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    navigator.serviceWorker
      .register(`${process.env.PUBLIC_URL}/service-worker.js`)
      .then(function (register) {
        console.log("worked", register);
      })
      .catch(function (err) {
        console.log("error!", err);
      });
  }
}
